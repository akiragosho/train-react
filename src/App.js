import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";


import Home from './pages/Home'

import Header from './components/Header'
import Products from './pages/Products'
import ToDoLists from './pages/ToDoLists'
import TrafficLight from './pages/TrafficLight'
import ChildrenLesson from './pages/ChildrenLesson'
import Cart from './pages/Cart'
import DemoRef from './pages/demo-ref'

import './App.css';


class App extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Router>
        <div className="App">
          <Header />
          <Route path="/" exact component={Home} />
          <Route path="/products" exact component={Products} />
          <Route path="/todolist" exact component={ToDoLists} />
        </div>
      </Router>

    );
  }
}

export default App;

